using UnityEngine;

[RequireComponent(typeof(Rigidbody2D), typeof(BoxCollider2D))]
public class SimpleMovement : MonoBehaviour {
    [Header("Configurations")] public Rigidbody2D rigidBody;
    public float movementSpeedX;
    public float movementSpeedY;
    public KeyCode keyUp;
    public KeyCode keyDown;
    public KeyCode keyLeft;
    public KeyCode keyRight;

    [Header("Runtime")] private Vector2 newVelocity;
    private float horizontalMovement;
    private float verticalMovement;

    private void Start() {}

    private void Update() {
        var isUp = Input.GetKey(keyUp);
        var isDown = Input.GetKey(keyDown);
        var isLeft = Input.GetKey(keyLeft);
        var isRight = Input.GetKey(keyRight);

        if (isUp && !isDown) {
            verticalMovement = 1f;
        } else if (isDown && !isUp) {
            verticalMovement = -1f;
        } else {
            verticalMovement = 0f;
        }

        if (isLeft && !isRight) {
            horizontalMovement = -1f;
        } else if (isRight && !isLeft) {
            horizontalMovement = 1f;
        } else {
            horizontalMovement = 0f;
        }

        newVelocity = Vector2.zero;
        newVelocity.x = horizontalMovement * movementSpeedX;
        newVelocity.y = verticalMovement * movementSpeedY;
    }

    private void FixedUpdate() {
        rigidBody.MovePosition(rigidBody.position + newVelocity * Time.fixedDeltaTime);
    }
}
