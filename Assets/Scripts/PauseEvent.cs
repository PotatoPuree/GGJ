using UnityEngine;

public class PauseEvent : MonoBehaviour {
    [Header("Runtime")]
    public bool ignorePause = false;

    public void OnPause() {
        if (!ignorePause) {
            Time.timeScale = 0f;
        }
    }

    public void OnResume() {
        Time.timeScale = 1f;
    }
}
