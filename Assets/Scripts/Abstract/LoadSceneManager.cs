using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadSceneManager : Singleton<LoadSceneManager>
{
    public enum Scenes
    {
        MainMenu,
        Game,
        Credits,
        Exit
    }
    
    public void LoadScene(Scenes sceneName)
    {
        SceneManager.LoadScene(sceneName.ToString());
    }

    public void AssignButtons(List<SceneData.ButtonsList> buttons)
    {
        foreach (var t in buttons)
        {
            if (t.sceneName == Scenes.Exit)
            {
                t.button.onClick.AddListener(() =>
                {
                    Debug.Log("Quit");
                    Application.Quit();
                });
                continue;
            }
            t.button.onClick.AddListener(() =>
            {
                Debug.Log("Load Scene: " + t.sceneName);
                LoadScene(t.sceneName);
                Time.timeScale = 1;
            });
        }
    }
}
