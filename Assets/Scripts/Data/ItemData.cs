using System.Collections.Generic;
using Gameplay.Interactable;
using UnityEngine;

namespace Data
{
    [CreateAssetMenu(fileName = "ItemData", menuName = "ScriptableObjects/ItemData", order = 1)]
    public class ItemData : ScriptableObject
    {
        public enum ItemType
        {
            Bread
        }
        public string itemName;
        public Sprite sprite;
        public ItemType itemType;
        public int requiredProgress;
        public Interactable.Stations station;
        public ItemData productItem;
        public bool finishedProduct;
    }
}
