using System;
using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneData : MonoBehaviour
{
    [SerializeField] private List<ButtonsList> buttons;

    private void Start()
    {
        LoadSceneManager.Instance.AssignButtons(buttons);
    }
    
    [Serializable]
    public class ButtonsList
    {
        [Required]
        public Button button;
        [Required]
        public LoadSceneManager.Scenes sceneName;
    }
}
