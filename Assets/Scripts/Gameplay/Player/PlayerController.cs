using System.Collections;
using MoreMountains.Feedbacks;
using MoreMountains.Tools;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Serialization;

[RequireComponent(typeof(Rigidbody2D), typeof(BoxCollider2D))]
public class PlayerController : MonoBehaviour
{
    Player player;
    [Required]
    [Header("Configurations")] public Rigidbody2D rigidBody;

    public float movementSpeed = 10f;
    public float activeMovementSpeed = 10f;

    public KeyCode keyUp;
    public KeyCode keyDown;
    public KeyCode keyLeft;
    public KeyCode keyRight;
    public KeyCode keyInteract;
    public KeyCode keyDash;
    public float dashSpeed = 20f;

    [Header("Runtime")] 
    private Vector2 newVelocity;
    private float horizontalMovement;
    private float verticalMovement;
    public float dashCounter = 1;
    public float dashCooldownCounter = 1;
    public float dashlength = 0.5f;
    public float dashCooldown = 1f;
    public bool isDashing;
    private Vector2 dashDirection;
    
    
    public MMFeedbacks playerDashFeedback;
    
    
    // Start is called before the first frame update
    private void Start()
    {
        player = GetComponent<Player>();
    }

    // Update is called once per frame
    private void Update()
    {
        Movement();
    }

    private void Movement()
    {
        if (player.isStunned)
        {
            return;
        }
        newVelocity = Vector2.zero;
        var isUp = Input.GetKey(keyUp);
        var isDown = Input.GetKey(keyDown);
        var isLeft = Input.GetKey(keyLeft);
        var isRight = Input.GetKey(keyRight);

        
        if (isUp && !isDown)
        {
            verticalMovement = 1f;
        }
        else if (isDown && !isUp)
        {
            verticalMovement = -1f;
        }
        else
        {
            verticalMovement = 0f;
        }


        if (isLeft && !isRight)
        {
            horizontalMovement = -1f;
        }
        else if (isRight && !isLeft)
        {
            horizontalMovement = 1f;
        }
        else
        {
            horizontalMovement = 0f;
        }

        
        newVelocity = new Vector2(horizontalMovement, verticalMovement);
        newVelocity.Normalize();
        
        if (Input.GetKeyDown(keyDash))
        {
            if (dashCounter <= 0 && dashCooldownCounter <= 0)
            {
                activeMovementSpeed = dashSpeed;
                dashCounter = dashlength;
                dashDirection = newVelocity; // Store the direction of the dash
            }
        }
        if (dashCounter > 0)
        {
            dashCounter -= Time.deltaTime;
            isDashing = true;
            playerDashFeedback.PlayFeedbacks();
            player.animator.SetBool("isDashing", true);
            if (dashCounter <= 0)
            {
                MMSoundManager.Instance.PlaySound(GameManager.Instance.soundData.Find(x => x.name == "Whosh"), MMSoundManager.MMSoundManagerTracks.Sfx, transform.position);
                activeMovementSpeed = movementSpeed;
                dashCooldownCounter = dashCooldown;
                isDashing = false;
                player.animator.SetBool("isDashing", false);
            }
        }
        if (dashCooldownCounter > 0)
        {
            dashCooldownCounter -= Time.deltaTime;
        }

        // Use dashDirection for the velocity during the dash
        if (isDashing)
        {
            rigidBody.velocity = dashDirection * activeMovementSpeed;
        }
        else
        {
            rigidBody.velocity = newVelocity * activeMovementSpeed;
        }
        
        if (newVelocity != Vector2.zero)
        {
            player.animator.SetBool("isRunning", true);
        }
        else
        {
            player.animator.SetBool("isRunning", false);
        }
        
    }
    
    
    
    
    
}