using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Gameplay;
using Gameplay.Interactable;
using MoreMountains.Feedbacks;
using MoreMountains.Tools;
using UnityEngine;

public class Player : MonoBehaviour
{
    public enum PlayerID
    {
        Player1,
        Player2
    }
    
    public PlayerID playerID;
    
    public Interactable interactable;
    public GameObject itemSlot;
    public float itemSlotOffset = 0.5f;
    public bool isStunned;
    public float stunDuration;
    public Item item;
    public Player otherPlayer;
    public Animator animator;
    
    List<AudioClip> randomSoundBark = new List<AudioClip>();
    List<AudioClip> randomSoundLaugh = new List<AudioClip>();
    

    
    private PlayerController playerController;

    public float
        interactionRange = 5f; // Set this to the maximum distance at which the player can interact with objects

    private List<Interactable> interactablesInRange = new();

    private void Start()
    {
        animator = GetComponent<Animator>();
        playerController = GetComponent<PlayerController>();
        otherPlayer = FindObjectsOfType<Player>().First(p => p != this);
        randomSoundBark.Add(GameManager.Instance.soundData.Find(x => x.name == "Bark 1"));
        randomSoundBark.Add(GameManager.Instance.soundData.Find(x => x.name == "Bark 2"));
        randomSoundBark.Add(GameManager.Instance.soundData.Find(x => x.name == "Bark 3"));
        randomSoundBark.Add(GameManager.Instance.soundData.Find(x => x.name == "Bark 4"));
        randomSoundLaugh.Add(GameManager.Instance.soundData.Find(x => x.name == "Laugh 1"));
        randomSoundLaugh.Add(GameManager.Instance.soundData.Find(x => x.name == "Laugh 2"));
        randomSoundLaugh.Add(GameManager.Instance.soundData.Find(x => x.name == "Laugh 3"));
    }

    private void Update()
    {
        // Clear the list and find all interactable objects within range
        interactablesInRange.Clear();
        interactablesInRange = FindObjectsOfType<Interactable>()
            .Where(i => Vector3.Distance(transform.position, i.transform.position) <= interactionRange)
            .ToList();

        // If the player has an item, remove it from the list of interactable objects
        if (item != null) interactablesInRange.Remove(item.GetComponent<Interactable>());
        if (otherPlayer.item != null) interactablesInRange.Remove(otherPlayer.item.GetComponent<Interactable>());

        // Check if there is an item on the station and remove it from the list of interactable objects
        foreach (var interactable in interactablesInRange.ToList())
        {
            if (interactable is MakeStation station && station.itemOnStation != null)
            {
                interactablesInRange.Remove(station.itemOnStation.GetComponent<Interactable>());
            }
        }

        // Sort the list based on distance to the player
        interactablesInRange.Sort((i1, i2) => Vector3.Distance(transform.position, i1.transform.position)
            .CompareTo(Vector3.Distance(transform.position, i2.transform.position)));

        // If there are any interactable objects within range, set the closest one as the current interactable object
        interactable = interactablesInRange.Count > 0 ? interactablesInRange[0] : null;

        // ...

        if (Input.GetKeyDown(playerController.keyInteract))
        {
            if (item != null && interactable == null)
            {
                DropItem(item);
            }
            else if (interactable != null)
            {
                Debug.Log("Interacting with " + interactable.name);
                interactable.Interact(this);
            } 
            
            
            if (Random.Range(1, 11) == 1)
                RandomSound(randomSoundBark);
        }
    }
    private void OnDrawGizmosSelected()
    {
        // Draw a sphere representing the interaction range
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, interactionRange);
    }

    public void DropItem(Item item)
    {
        Debug.Log("Dropping item");
        item.transform.SetParent(null);
        item.tag = "Interactable";
        this.item = null;
        animator.SetBool("isHolding", false);
    }

    public void PickupItem(Item item)
    {
        item.HideProgressBar();
        animator.SetBool("isHolding", true);
        Debug.Log("Picking up item");
        item.transform.SetParent(itemSlot.transform);
        item.transform.localPosition = Vector3.zero;
        this.item = item;
        item.tag = "Untagged";
    }


    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player") && playerController.isDashing)
        {
            Debug.Log("Player collided with player");
            var player2 = other.gameObject.GetComponent<Player>();
            if (player2.item != null)
            {
                Debug.Log("Steal Item");
                if (item == null)
                {
                    Debug.Log("Player collided with player holding item and picked it up");
                    PickupItem(player2.item);
                    player2.item = null;
                    player2.animator.SetBool("isHolding", false);
                    RandomSound(randomSoundLaugh);
                    
                }
                // TODO: Might Change Later
            }

            player2.Stun(stunDuration);
            MMSoundManager.Instance.PlaySound(GameManager.Instance.soundData.Find(x => x.name == "Stunt 1"),
                MMSoundManager.MMSoundManagerTracks.Sfx, transform.position);
        }
    }

    void RandomSound(List<AudioClip> clips)
    {
        int randomIndex = Random.Range(0, clips.Count);
        MMSoundManager.Instance.PlaySound(clips[randomIndex], MMSoundManager.MMSoundManagerTracks.Sfx, transform.position);
    }
    
    public void Stun(float duration)
    {
        StartCoroutine(StunCoroutine(duration));
    }

    private IEnumerator StunCoroutine(float duration)
    {
        isStunned = true;
        animator.SetBool( "isStunned", true );
        playerController.enabled = false;
        yield return new WaitForSeconds(duration);
        playerController.enabled = true;
        isStunned = false;
        animator.SetBool( "isStunned", false );
    }
}