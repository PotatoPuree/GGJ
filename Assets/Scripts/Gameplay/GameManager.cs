using System;
using System.Collections.Generic;
using Data;
using MoreMountains.Tools;
using UnityEngine;
using TMPro;
using Unity.VisualScripting;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [Serializable]
    public class Objective
    {
        public ItemData.ItemType itemType;
        public int progress;
        public int requiredProgress;
        public bool completed;
    }
    
    public float timeLimit;
    
    public CountdownTimer timer;
    
    
    public List<Objective> objectiveP1;
    public List<Objective> objectiveP2;

    public List<AudioClip> soundData;
    //UI
    public TextMeshProUGUI p1ScoreText;
    public TextMeshProUGUI p2ScoreText;
    

    public static GameManager Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void Start()
    {
        timer = GetComponent<CountdownTimer>();
        UpdateText(Player.PlayerID.Player1);
        UpdateText(Player.PlayerID.Player2);
        timer.SetTimeRemaining(timeLimit);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !timer.isCounting)
        {
            timer.TimeStart();
        }
    }

    public void StartTimerSound()
    {
        MMSoundManager.Instance.PlaySound(soundData.Find(x => x.name == "TimerNormal"),
            MMSoundManager.MMSoundManagerTracks.Sfx, transform.position, loop:true);
    }
    
    public void StopTimerSound()
    {
        MMSoundManager.Instance.StopSound(MMSoundManager.Instance.FindByClip(soundData.Find(x => x.name == "TimerNormal")));
    }

    public void AddBread(Player.PlayerID player, ItemData itemData)
    {
        if (player == Player.PlayerID.Player1)
        {
            foreach (var VARIABLE in objectiveP1)
            {
                if (VARIABLE.itemType == itemData.itemType)
                {
                    VARIABLE.progress++;
                    UpdateText(player);
                    if (VARIABLE.progress >= VARIABLE.requiredProgress)
                    {
                        VARIABLE.completed = true;
                    }
                }
            }
            if (objectiveP1.TrueForAll(o => o.completed))
            {
                PlayerPrefs.SetInt("winner", 1);
                SceneManager.LoadScene("WinLose");
                Debug.Log("Player 1 wins");
            }
            
        }
        else 
        {
            foreach (var VARIABLE in objectiveP2)
            {
                if (VARIABLE.itemType == itemData.itemType)
                {
                    VARIABLE.progress++;
                    UpdateText(player);
                    if (VARIABLE.progress >= VARIABLE.requiredProgress)
                    {
                        VARIABLE.completed = true;
                    }
                }
            }
            if (objectiveP2.TrueForAll(o => o.completed))
            {
                PlayerPrefs.SetInt("winner", 2);
                SceneManager.LoadScene("WinLose");
                Debug.Log("Player 2 wins");
            }
        }
    }

    void UpdateText(Player.PlayerID player)
    {
        if (player == Player.PlayerID.Player1)
        {
            p1ScoreText.text = "";
            foreach (var VARIABLE in objectiveP1)
            {
                p1ScoreText.text += VARIABLE.itemType + ": " + VARIABLE.progress + "/" + VARIABLE.requiredProgress + "\n";
            }
        }
        else
        {
            p2ScoreText.text = "";
            foreach (var VARIABLE in objectiveP2)
            {
                p2ScoreText.text += VARIABLE.itemType + ": " + VARIABLE.progress + "/" + VARIABLE.requiredProgress + "\n";
            }
        }
    }
}