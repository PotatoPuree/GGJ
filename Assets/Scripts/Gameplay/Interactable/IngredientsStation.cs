﻿using Data;
using MoreMountains.Tools;
using Sirenix.OdinInspector;
using UnityEngine;

namespace Gameplay.Interactable
{
    public class IngredientsStation : Interactable
    {
        public int amount;
        public ItemData itemData;
        [Required]
        [SerializeField] private Item itemPrefab;
        
        public override void Interact(Player player)
        {
            if (amount > 0)
            {
                // if player has no item
                if (player.item == null)
                {
                    // give player item
                    
                    player.item = Instantiate(itemPrefab, player.itemSlot.transform);
                    player.item.AssignItemData(itemData);
                    player.PickupItem(player.item);
                    MMSoundManager.Instance.PlaySound(GameManager.Instance.soundData.Find(x => x.name == "Pop"),
                        MMSoundManager.MMSoundManagerTracks.Sfx, transform.position);
                    Debug.Log("IngredientsStation");
                    amount--;
                }
                else
                {
                    Debug.Log("Player already has item");
                }
                
                
            }
            else
            {
                Debug.Log("No ingredients left");
            }
        }
    }
}