﻿using System;
using Data;
using UnityEngine;

namespace Gameplay.Interactable
{
    public class MakeStation : Interactable
    {
        public Item itemOnStation;
        public GameObject itemSlot;
        public float progressPerSecond = 1f;
        public Animator animator;

        private void Start()
        {
            itemSlot = transform.GetChild(0).gameObject;
            if(animator == null)
            {
                animator = GetComponent<Animator>();
            }
        }

        public override void Interact(Player player)
        {
            Debug.Log("Interact method called");
            if (player.item == null && itemOnStation == null)
                return;
            

            if (player.item != null && itemOnStation != null)
                return;

            if (player.item != null)
            {
                if (player.item.station == station)
                {
                    animator.speed = progressPerSecond;
                    MoveToStation();
                }
            }
                
            
            else if (itemOnStation != null)
            {
                player.PickupItem(itemOnStation);
                Debug.Log("Picked up item from station");
                itemOnStation = null;
                Debug.Log(itemOnStation);
            }
            

            return;

            void MoveToStation()
            {
                itemOnStation = player.item;
                itemOnStation.ShowProgressBar();
                player.item = null;
                player.animator.SetBool("isHolding", false);
                itemOnStation.transform.SetParent(itemSlot.transform);
                itemOnStation.transform.localPosition = Vector3.zero;
                itemOnStation.transform.localRotation = Quaternion.Euler(Vector3.zero);
            }
        }
        

        private void Update()
        {
            if (itemOnStation != null && itemOnStation.station == station)
            {
                itemOnStation.IncreaseProgress( progressPerSecond * Time.deltaTime);
            }
            else
            {
                animator.speed = 0f;
                animator.Rebind();
            }
        }
    }
}