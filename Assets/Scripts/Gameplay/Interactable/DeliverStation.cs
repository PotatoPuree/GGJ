﻿using Data;
using MoreMountains.Tools;
using UnityEngine;

namespace Gameplay.Interactable
{
    public class DeliverStation : Interactable
    {
        
        public override void Interact(Player player)
        {
            if (player.item == null)
            {
                return;
            }
            if (player.item.station == station)
            {
                // Add code here to interact with the station
                Debug.Log("No item to deliver");
                if (player.item._finishedProduct)
                {
                    Debug.Log("Delivered item");
                    Destroy(player.item.gameObject);
                    GameManager.Instance.AddBread(player.playerID, player.item.itemData);
                    player.item = null;
                    player.animator.SetBool("isHolding", false);
                    MMSoundManager.Instance.PlaySound(GameManager.Instance.soundData.Find(x => x.name == "Delivered"),
                        MMSoundManager.MMSoundManagerTracks.Sfx, transform.position);
                }
            }
        }
    }
}