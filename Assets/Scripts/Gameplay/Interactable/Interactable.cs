using System;
using System.Collections.Generic;
using Data;
using MoreMountains.Feedbacks;
using UnityEngine;

namespace Gameplay.Interactable
{
    public abstract class Interactable : MonoBehaviour
    {
        
        public enum Stations
        {
            Dough,
            DoughMaker,
            Oven,
            Deliver
        }
        

        public Stations station;

        public abstract void Interact(Player player);
    }
}
