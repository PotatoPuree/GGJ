﻿using System;
using System.Collections.Generic;
using Data;
using MoreMountains.Tools;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;


namespace Gameplay
{
    /// <summary>
    /// Represents an item in the game.
    /// </summary>
    public class Item : Interactable.Interactable
    {
        public ItemData itemData;
        public ItemData productItem;
        public float progress;

        private SpriteRenderer _spriteRenderer;
        public int requiredProgress;
        public ItemData.ItemType itemType;
        private Slider _progressbar;
        public bool _finishedProduct = false;

        /// <summary>
        /// Awake is called when the script instance is being loaded.
        /// </summary>
        private void Awake()
        {
            _spriteRenderer = GetComponent<SpriteRenderer>();
            _progressbar = GetComponentInChildren<Slider>();
        }

        /// <summary>
        /// Start is called before the first frame update.
        /// </summary>
        private void Start()
        {
            
            DisplayItem();
        }

        /// <summary>
        /// Assigns the item data to this item.
        /// </summary>
        /// <param name="itemData">The item data to assign.</param>
        public void AssignItemData(ItemData itemData)
        {
            this.itemData = itemData;
            DisplayItem();
        }

        /// <summary>
        /// Displays the item.
        /// </summary>
        private void DisplayItem()
        {
            _spriteRenderer.sprite = itemData.sprite;
            requiredProgress = itemData.requiredProgress;
            progress = 0;
            station = itemData.station;
            _progressbar.maxValue = requiredProgress;
            _progressbar.value = progress;
            productItem = itemData.productItem;
            _finishedProduct = itemData.finishedProduct;
        }

        public void ShowProgressBar()
        {
            _progressbar.gameObject.SetActive(true);
        }
        
        public void HideProgressBar()
        {
            _progressbar.gameObject.SetActive(false);
        }
        
        /// <summary>
        /// Decreases the progress of the item.
        /// </summary>
        /// <param name="amount">The amount to increase by. Default is 1.</param>
        public void IncreaseProgress(float amount = 1)
        {
            progress += amount;
            CheckProgress();
            _progressbar.value = progress;
            
        }

        /// <summary>
        /// Checks the progress of the item.
        /// </summary>
        private void CheckProgress()
        {
            progress = Mathf.Clamp(progress, 0, requiredProgress);
            if (progress >= requiredProgress)
            {
                if (productItem != null)
                {
                    if (station == Stations.Oven)
                    {
                        MMSoundManager.Instance.PlaySound(GameManager.Instance.soundData.Find(x => x.name == "Ping"),
                            MMSoundManager.MMSoundManagerTracks.Sfx, transform.position);
                    }
                    else if (station == Stations.DoughMaker)
                    {
                        MMSoundManager.Instance.PlaySound(GameManager.Instance.soundData.Find(x => x.name == "Ping"), 
                            MMSoundManager.MMSoundManagerTracks.Sfx, transform.position, pitch:0.5f);
                    }
                    
                    itemData = productItem;
                    if (itemData.finishedProduct)
                    {
                        _finishedProduct = true;
                        _progressbar.gameObject.SetActive(false);
                    }
                    DisplayItem();
                }
            }
        }

        public override void Interact(Player player)
        {  
            if (player.item == null)
            {
                player.PickupItem( this);
            }
                
        }
    }
}