using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;

public class SettingUI_Menu_OnMouseOver : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    private TextMeshProUGUI text;
    private string originalText;

    void Start() {
        text = GetComponentInChildren<TextMeshProUGUI>();
        originalText = text.text;
    }

    public void OnPointerEnter(PointerEventData pointerEventData) {
        text.text = ">" + originalText;
    }

    public void OnPointerExit(PointerEventData pointerEventData) {
        text.text = originalText;
    }
}
