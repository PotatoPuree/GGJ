using TMPro;
using UnityEngine;

public class SettingUI_Keyboard_Button_Change_Kaymap : MonoBehaviour {
    public int PlayerNumber;
    public int KeyIndex;
    public Settings settings;

    public bool isChange;

    void Update() {
        if (isChange) {
            KeyCode kc = settings.InputListen();
        
            if (kc != KeyCode.None) {
                settings.keyboard_keymap[PlayerNumber][KeyIndex] = kc;
                GetComponentInChildren<TextMeshProUGUI>().text = kc.ToString();
                isChange = false;
                settings.ObjectTextPressKey.gameObject.SetActive(false);
            }
        }
    }

    public void OnClick() {
        settings.ObjectTextPressKey.gameObject.SetActive(true);
        isChange = true;
    }
}
