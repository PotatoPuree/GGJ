using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class SettingWinUI : MonoBehaviour
{
    [SerializeField] private TMP_Text win;
    [SerializeField] private TMP_Text lose;
    int playerwin;
    // Start is called before the first frame update
    void Start()
    {
        playerwin = PlayerPrefs.GetInt("winner");
        win.text = $"Player {(playerwin == 1 ? 1 : 2)} Win";
        lose.text = $"Player {(playerwin == 1 ? 2 : 1)} Lose";
    }

}
