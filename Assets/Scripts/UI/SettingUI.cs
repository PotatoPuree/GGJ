using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Events;
using UnityEngine.UI;

public class Settings : MonoBehaviour
{
    public enum UIMode { GamePlay, MainMenu }

    [Header("Configurations")]
    public UIMode InterfaceMode;
    public Canvas ObjectSettingUI;
    public GameObject ButtonResume;
    public GameObject ObjectGraphicsQuality;
    public GameObject ObjectAudio;
    public GameObject ObjectKeyboard;
    public TextMeshProUGUI ObjectTextTitle;
    public TextMeshProUGUI ObjectTextVolumePercent;
    public Toggle toggleFullScreenMode;
    public Slider sliderVolume;
    public AudioMixer audioMixer;
    public TextMeshProUGUI ObjectTextPressKey;
    public Button KeymapP10;
    public Button KeymapP11;
    public Button KeymapP12;
    public Button KeymapP13;
    public Button KeymapP14;
    public Button KeymapP15;
    public Button KeymapP20;
    public Button KeymapP21;
    public Button KeymapP22;
    public Button KeymapP23;
    public Button KeymapP24;
    public Button KeymapP25;
    public Image imageDimming;
    public Button[] Keymaps;

    [Header("Runtime")]
    public bool isOpen = false;
    public bool ignoreEsc = false;

    [Header("Events")]
    public UnityEvent OnPause;
    public UnityEvent OnResume;
    public UnityEvent OnExit;

    [SerializeField] public TMP_Dropdown resolutionDropdown;

    private List<Resolution> screenResolutions = new();
    private List<string> screenResolutionOptions = new();
    private int currentResolutionIndex = 0;

    private string MenuTitleName;
    public KeyCode[][] keyboard_keymap = new KeyCode[][] {
        new KeyCode[] { KeyCode.None, KeyCode.None, KeyCode.None, KeyCode.None, KeyCode.None, KeyCode.None },
        new KeyCode[] { KeyCode.None, KeyCode.None, KeyCode.None, KeyCode.None, KeyCode.None, KeyCode.None }
    };

    // Start is called before the first frame update
    void Start()
    {
        Keymaps = new Button[] { KeymapP10, KeymapP11, KeymapP12, KeymapP13, KeymapP14, KeymapP15, KeymapP20, KeymapP21, KeymapP22, KeymapP23, KeymapP24, KeymapP25 };

        ObjectSettingUI.enabled = false;
        if (InterfaceMode == UIMode.GamePlay) {
            MenuTitleName = "Game Pause";
        } else if (InterfaceMode == UIMode.MainMenu) {
            MenuTitleName = "Settings";
            ButtonResume.SetActive(false);
        }

        ObjectTextTitle.text = MenuTitleName;

        OnResume ??= new UnityEvent();
        OnPause ??= new UnityEvent();
        OnExit ??= new UnityEvent();

        Load();
    }

    void Load() {
        float volume;
        audioMixer.GetFloat("MasterVolume", out volume);
        volume += 80;
        ObjectTextVolumePercent.text = ((int)(volume)).ToString() + "%";
        sliderVolume.value = volume / 100;
        KeymapUpdateScreen();
        toggleFullScreenMode.isOn = Screen.fullScreen;
        screenResolutions.Clear();
        screenResolutionOptions.Clear();
        Resolution[] resolutions = Screen.resolutions;
        resolutionDropdown.ClearOptions();

        for (int i = 0; i < resolutions.Length; i++)
        {
            string nameOption = resolutions[i].width.ToString() + "x" + resolutions[i].height.ToString();
            if (!screenResolutionOptions.Exists(option => option == nameOption))
            {
                screenResolutions.Add(resolutions[i]);
                screenResolutionOptions.Add(nameOption);
            }

            if (resolutions[i].width == Screen.width && resolutions[i].height == Screen.height)
            {
                currentResolutionIndex = i;
            }
        }

        resolutionDropdown.AddOptions(screenResolutionOptions);
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();
    }

    // Update is called once per frame
    void Update()
    {
        if (ignoreEsc) {
            ObjectSettingUI.enabled = false;
            isOpen = false;
        } else {
            if (Input.GetKeyDown(KeyCode.Escape)) {
                isOpen = !isOpen;

                if (isOpen) {
                    OnEvent_Pause();
                } else {
                    OnClick_Resume();
                }
            }
        }
    }

    public void SetResolution(int resolutionIndex) {
        currentResolutionIndex = resolutionIndex;
        Resolution resolution = screenResolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreenMode, resolution.refreshRateRatio);
    }

    public void OnEvent_Pause() {
        imageDimming.rectTransform.anchorMin = Vector2.zero;
        imageDimming.rectTransform.anchorMax = Vector2.one;
        imageDimming.rectTransform.offsetMin = Vector2.zero;
        imageDimming.rectTransform.offsetMax = Vector2.zero;
        imageDimming.enabled = true;
        Load();
        ObjectSettingUI.enabled = true;
        isOpen = true;
        OnPause.Invoke();
        ObjectGraphicsQuality.SetActive(false);
        ObjectAudio.SetActive(false);
        ObjectKeyboard.SetActive(false);
    }

    public void OnClick_Resume() {
        imageDimming.enabled = false;
        ObjectSettingUI.enabled = false;
        isOpen = false;
        OnResume.Invoke();
        ObjectGraphicsQuality.SetActive(false);
        ObjectAudio.SetActive(false);
        ObjectKeyboard.SetActive(false);
        ObjectTextTitle.text = MenuTitleName;
    }

    public void OnClick_GraphicsQuality() {
        ObjectGraphicsQuality.SetActive(true);
        ObjectAudio.SetActive(false);
        ObjectKeyboard.SetActive(false);
        ObjectTextTitle.text = MenuTitleName + " > Graphics Quality";
    }
    
    public void OnClick_Audio() {
        ObjectGraphicsQuality.SetActive(false);
        ObjectAudio.SetActive(true);
        ObjectKeyboard.SetActive(false);
        ObjectTextTitle.text = MenuTitleName + " > Audio";
    }

    public void OnClick_Keyboard() {
        ObjectGraphicsQuality.SetActive(false);
        ObjectAudio.SetActive(false);
        ObjectKeyboard.SetActive(true);
        ObjectTextTitle.text = MenuTitleName + " > Keyboard";
    }
    
    public void OnClick_Exit() {
        OnExit.Invoke();
    }

    public void SetVolume(float volume) {
        volume *= 100;
        audioMixer.SetFloat("MasterVolume", volume - 80);
        ObjectTextVolumePercent.text = ((int)(volume)).ToString() + "%";
    }

    public void SetFullScreenMode(bool fullscreen) {
        Screen.fullScreen = fullscreen;
    }

    public void SetGraphicsQuality(int qualityIndex) {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    public void SetDefaultKey() {
        keyboard_keymap[0][0] = KeyCode.W;
        keyboard_keymap[0][1] = KeyCode.S;
        keyboard_keymap[0][2] = KeyCode.A;
        keyboard_keymap[0][3] = KeyCode.D;
        keyboard_keymap[0][4] = KeyCode.LeftShift;
        keyboard_keymap[0][5] = KeyCode.LeftAlt;

        keyboard_keymap[1][0] = KeyCode.UpArrow;
        keyboard_keymap[1][1] = KeyCode.DownArrow;
        keyboard_keymap[1][2] = KeyCode.LeftArrow;
        keyboard_keymap[1][3] = KeyCode.RightArrow;
        keyboard_keymap[1][4] = KeyCode.RightShift;
        keyboard_keymap[1][5] = KeyCode.RightControl;
    }

    public KeyCode InputListen() {
        if (Input.anyKeyDown) {
            foreach (KeyCode keyCode in System.Enum.GetValues(typeof(KeyCode))) {
                if (Input.GetKeyDown(keyCode)) {
                    if (keyCode == KeyCode.Escape || keyCode == KeyCode.Mouse0 || keyCode == KeyCode.Mouse1) {
                        break;
                    }

                    return keyCode;
                }
            }
        }
        
        return KeyCode.None;
    }

    public void KeymapUpdateScreen() {
        int k = 0;
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 6; j++) {
                Keymaps[k].GetComponentInChildren<TextMeshProUGUI>().text = keyboard_keymap[i][j].ToString();
                k++;
            }
        }
    }

    public void OnClick_DefaultKeymap() {
        SetDefaultKey();
        KeymapUpdateScreen();
    }

    public void ClearKeyboardWaiting() {
        ObjectTextPressKey.gameObject.SetActive(false);
        for (int i = 0; i < Keymaps.Length; i++) {
            Keymaps[i].GetComponent<SettingUI_Keyboard_Button_Change_Kaymap>().isChange = false;
        }
    }
}
