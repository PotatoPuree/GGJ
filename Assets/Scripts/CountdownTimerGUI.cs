using Sirenix.OdinInspector;
using UnityEngine;
using TMPro;
using UnityEngine.Events;


public class CountdownTimer : MonoBehaviour
{
    [Header("References")]
    [Required]
    public TextMeshProUGUI textUI;
    public UnityEvent OnTimeout;

    [Header("Runtime")]
    // Optimize for text output
    private string strHead;
    private string strTail;

    public bool isCounting {get; private set;}
    private float endTime = 0;
    private float timeRemaining = 0;
    private int pauseTime = 0; // Fixed time to display when timer is paused.
    

    // Start is called before the first frame update
    void Start()
    {
        string delimiter = "{TIME}";
        string[] parts = textUI.text.Split(new[] { delimiter }, 2, System.StringSplitOptions.None);
        strHead = parts[0];
        if (parts.Length > 1)
        {
            strTail = parts[1];
        }
        else
        {
            strTail = ""; // Or provide a default value
        }

        if (OnTimeout == null) {
            OnTimeout = new UnityEvent();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (isCounting) {
            timeRemaining = endTime - Time.time;
            int time = (int)(timeRemaining);

            // It comes true when time ends. Then it will be called back when there is a callback function
            if (time <= 0) {
                time = 0;
                pauseTime = 0;
                isCounting = false;
                OnTimeout.Invoke();
            }

            textUI.text = strHead + time.ToString() + strTail;
        } else {
            // When time runs out or time is paused
            textUI.text = strHead + pauseTime.ToString() + strTail;
        }
    }

    // For set the time to countdown
    public void SetTimeRemaining(float second) {
        timeRemaining = second;
        endTime = timeRemaining + Time.time;
    }

    // Start
    public void TimeStart() {
        Debug.Log("TimeStart");
        endTime = Time.time + timeRemaining;
        isCounting = true;
        GameManager.Instance.StartTimerSound();
    }

    // Pause
    public void TimePause() {
        timeRemaining = endTime - Time.time;
        pauseTime = (int)(timeRemaining);
        isCounting = false;
    }

    // Countdown resumes after pause
    public void TimeResume() {
        endTime = Time.time + timeRemaining;
        isCounting = true;
    }

    // Add time while counting down
    public void TimeAdd(float second) {
        timeRemaining = endTime - Time.time;
        endTime = timeRemaining + second;
    }

    // Reduce time while counting down
    public void TimeReduce(float second) {
        timeRemaining = endTime - Time.time;
        endTime = timeRemaining - second;
        
        if (endTime <= 0) {
            pauseTime = 0;
            isCounting = false;
            OnTimeout.Invoke();
        }
    }

    // Reset state
    public void TimeReset() {
        pauseTime = 0;
        isCounting = false;
        timeRemaining = 0;
        endTime = 0;
    }
}
